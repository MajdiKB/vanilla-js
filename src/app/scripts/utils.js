var showSection = (event) => {

    //esto es para mostrar la sección q picamos unicamente

    //sacamos el nombre del boton
    var namebtn = event.srcElement.id.replace('btn','');
    //sacamos el nombre del div q coincida con el nombre del boton
    var div = document.getElementById(namebtn);
    //sacamos todos los botones con el buttons style
    var btn = document.getElementsByClassName("data__buttons-style");
    //vemos si el div q queremos mostrar tiene la clase show
    var test=div.className.includes('show');
    //si tiene la clase show
    if (test){
        div.classList.remove('show');
        //para quitar el color rosa del boton
        for (var item of btn){
            item.classList.remove('color-pink');
        }
    }
    //si no tiene la clase show
    else{
        var list = document.getElementsByClassName("articles");
        //esto es para ocultar todas las secciones
        for (var item of list){
            item.classList.add('hide');
            item.classList.remove('show');
            //para quitar el color rosa del boton
            for (var item2 of btn){
                item2.classList.remove('color-pink');
            }
        }
        div.classList.add('show');
        //para colorear el botón q hemos seleccionado
        var btn = document.getElementById(event.srcElement.id)
        btn.classList.add('color-pink');
    }
}

export {showSection};