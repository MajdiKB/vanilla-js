var experience = [
  {
    nameJob: "Freelance",
    companyName: "Majdi Kokaly",
    date: '2020 - Now',
  },
  {
    nameJob: "Gerente de Compras y Ventas",
    companyName: "Nabil M3",
    date: '2006 - 2020',
  }
];

var onLoadExperience = () => {

   var experienceList = document.getElementById("experienceList");
   var experienceTittle = document.getElementById("experienceTittle");
   experienceTittle.innerText = "Experiencia:";
   experienceTittle.classList.add("strong")
   experience.forEach((job) => {
   var li = document.createElement('li');
   var li2 = document.createElement('li');
   li.innerHTML = job.nameJob + ' | ' + job.companyName;
   li.classList.add("color-white");
   li2.innerHTML = job.date;
   experienceList.appendChild(li);
   experienceList.appendChild(li2);
});
}

export { onLoadExperience };
