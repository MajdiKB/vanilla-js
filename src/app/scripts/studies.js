var education = [ 
 {
    nameStudies: "Full Stack Developer.",
    placeStudies: "Upgrade.",
    yearStudies: "2021"
 },
 {
    nameStudies: "Desarrollo de app con tecnologías web.",
    placeStudies : "Centro Nacional Politécnico.",
    yearStudies: "2020/2021"
 },
 {
    nameStudies: "Desarrollo de app informáticas.",
    placeStudies : "I.E.S Vecindario.",
    yearStudies: "2003/2005"
 }
];

var onLoadEducation = () => {
    var educationList = document.getElementById("educationList");
    var educationTittle = document.getElementById("educationTittle");
    educationTittle.innerText = "Formación:";
    educationTittle.classList.add("strong");
    education.forEach((item) => {
    var li = document.createElement('li');
    var li2 = document.createElement('li');
    var li3 = document.createElement('li');
   //  li.setAttribute('class', 'experience'); preguntar a Mario
    li.innerHTML = item.nameStudies;
    li.classList.add("color-white");
    li2.innerHTML = item.placeStudies;
    li2.classList.add("strong");
    li3.innerHTML = item.yearStudies;
    educationList.appendChild(li);
    educationList.appendChild(li2);
    educationList.appendChild(li3);
 });
 }

export { onLoadEducation };
