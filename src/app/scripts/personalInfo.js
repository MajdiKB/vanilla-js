var personalInfo = {
  name: "Majdi Kokaly",
  mail: "majdi.kokaly@bootcamp_upgrade.com",
  phone: "670670670",
  photo: "https://keepcoding.io/wp-content/uploads/2020/12/tipos-de-programadores-el-novato-full-stack-450x450.png",
};

var onLoadPersonalInfo = () => {
    var myTittle = document.getElementById("personalInfoTittle");
    myTittle.innerText = "Datos Personales:"
    myTittle.classList.add("strong");
    var myName = document.getElementById("personalInfoName");
    myName.classList.add("italic");
    myName.innerHTML = "Nombre: " + personalInfo.name;

    var myMail = document.getElementById("personalInfoMail");
    myMail.classList.add("color-white");
    myMail.innerHTML = "E-mail: " + personalInfo.mail;

    var myPhone = document.getElementById("personalInfoPhone");
    myPhone.classList.add("italic");
    myPhone.innerHTML = "Tlf: " + personalInfo.phone;

    var myPhoto = document.getElementById("personalInfoPhoto");
    myPhoto.classList.add("height-max-foto");
    myPhoto.src = personalInfo.photo;
}


//menos eficiente

// var togglePersonalInfo = () => {

//   var personalInfo = document.getElementById("personal_info");

//   var education = document.getElementById("education");
//   var experience = document.getElementById("experience");
//   var language = document.getElementById("language");

//   personalInfo.classList.toggle("hide");

//   experience.classList.add("hide");
//   education.classList.add("hide");
//   language.classList.add("hide");
//   var btnPersonalInfo = document.getElementById("btnPersonalInfo");
//   btnPersonalInfo.classList.toggle("btn-active--color");

// }

export {onLoadPersonalInfo};
