var language = [
    {
      language: "Español",
      level: "* * * * *",
      flag: "https://images.vexels.com/media/users/3/164599/isolated/preview/ce858535b77f22068049aca2457e59ad-c-iacute-rculo-de-icono-de-idioma-de-bandera-de-espa-ntilde-a-by-vexels.png",
    },
    {
      language: "Inglés",
      level: "* * *",
      flag: "https://image.flaticon.com/icons/png/512/323/323329.png",
    },
    {
      language: "Árabe",
      level: "* * *",
      flag: "https://cdn.icon-icons.com/icons2/107/PNG/512/palestinian_territory_flag_flags_18114.png",
    },
    {
      language: "Chino Mandarín",
      level: "* * * * *",
      flag: "https://setidiomas.com/wp-content/uploads/2018/12/china-4.png",
    }
  ];

  var onLoadLanguage = () => {

     var languageList = document.getElementById("languageList");
     var languageTittle = document.getElementById("languageTittle");
    //  var languageFlag =  document.getElementById("flag");
     languageTittle.innerText = "Idiomas:";
     languageTittle.classList.add("strong");
     language.forEach((item) => {
      var span = document.createElement('span');
      var img = document.createElement('img');
      var li2 = document.createElement('li');
      span.innerHTML = item.language + " ";
      span.classList.add("color-white");
      img.src = item.flag;
      img.classList.add("language-flag--size");
      li2.innerHTML = "Nivel: " + item.level;
      languageList.appendChild(span);
      languageList.appendChild(img);
      languageList.appendChild(li2);
  });
  }

  export { onLoadLanguage };
