//lo q hay comentado es como se haría de otro modo menos eficiente

import { onLoadPersonalInfo} from './scripts/personalInfo';
import { onLoadExperience} from './scripts/experience';
import { onLoadEducation} from './scripts/studies';
import { onLoadLanguage} from './scripts/language';
// import { onLoadLanguage, toggleLanguage } from './scripts/language';
import {showSection} from './scripts/utils';

import './styles/styles.scss';

var addListeners = () =>{
    document.getElementById("btnpersonalInfo").addEventListener("click", showSection);
    document.getElementById("btnexperience").addEventListener("click", showSection);
    document.getElementById("btneducation").addEventListener("click", showSection);
    document.getElementById("btnlanguage").addEventListener("click", showSection);
    // document.getElementById("btnLanguage").addEventListener("click", toggleLanguage);
}

window.onload = () => {
    //Init Content
    onLoadPersonalInfo();
    onLoadExperience();
    onLoadEducation();
    onLoadLanguage();
    //
    addListeners();
};